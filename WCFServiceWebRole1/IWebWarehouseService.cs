﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace WarehouseService
{
    [ServiceContract]
    public interface IWebWarehouseService
    {
        [OperationContract]
        [WebGet(UriTemplate = "")]
        void redirection();
    }
}
