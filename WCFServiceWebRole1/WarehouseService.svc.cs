﻿using ModelDLL;
using NServiceBus;
using ServiceBusDLL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;

namespace WarehouseService
{
    public abstract class WarehouseService : 
        IWebWarehouseService, 
        IRestWarehouseService,
        IGetServiceBus
    {
        public abstract IEndpointInstance NServiceBusEndpoint { get; set; }

        public WarehouseService()
        {
            createTablesIfNotExist();
        }

        private void createTablesIfNotExist()
        {
            if (ModelOperations.CheckIfTableExists(WarehouseBook.TableName, new DatabaseConnection()) == false)
            {
                recreateTables();
            }
        }

        public void redirection()
        {
            var ctx = WebOperationContext.Current.OutgoingResponse;
            ctx.Location = "rest/index.html";
            ctx.StatusCode = System.Net.HttpStatusCode.Redirect;
        }
        //endpoint.Behaviors.Add(new WebHttpBehavior());
        private bool isAuthentificated()
        {
            bool isAuthentificated = false;
            WebOperationContext webOperationContext = WebOperationContext.Current;
            if (webOperationContext != null)
            {
                var sessionUUID = webOperationContext.IncomingRequest.Headers.Get("Authorization");

                if (sessionUUID != null)
                {

                    sessionUUID = sessionUUID.Replace("\\", "").Replace("\"", "");
                    try
                    {
                        var userService = UserServiceConnector.createInstance();
                        using ((System.IDisposable)userService)
                        {
                            isAuthentificated = userService.isSignedIn(Guid.Parse(sessionUUID));
                        }
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }

            }
            return isAuthentificated;        

        }

        public void recreateTables()
        {
            //drop tables
            ModelOperations.DropTableIfExists(WarehouseBook.TableName, new DatabaseConnection());           

            //create tables
            ModelOperations.ExecuteSqlCommand(WarehouseBook.CreateCommand, new DatabaseConnection());
            
            //insert mock values into tables
            ModelOperations.ExecuteSqlCommand(WarehouseBook.MockValuesCommand, new DatabaseConnection());

            ModelOperations.ExecuteSqlCommand("DELETE FROM WarehouseService_ReservationSaga", new ServiceBusDLL.SagasDatabaseConnection());
        }

        public Stream getLoginPage()
        {
            return getStreamFromFile(ServerConfiguration.basePath + "login.html", GuiDLL.GuiConstants.HTML_TYPE);
        }

        public Stream getJsFile(string jsFileName)
        {
            return getStreamFromFile(ServerConfiguration.basePath + "assets\\js\\" + jsFileName, GuiDLL.GuiConstants.JS_TYPE);
        }

        public Stream getCssFile(string cssFile)
        {
            return getStreamFromFile(ServerConfiguration.basePath + "assets\\css\\" + cssFile, GuiDLL.GuiConstants.CSS_TYPE);
        }

        public Stream getImageFile(string imageFile)
        {
            return getStreamFromFile(ServerConfiguration.basePath + "images\\" + imageFile, GuiDLL.GuiConstants.IMG_TYPE);
        }

        public Stream getFont1()
        {
            return getStreamFromFile(ServerConfiguration.basePath + "assets\\fonts\\fontawesome-webfont.woff", "application/font-woff");
        }

        public Stream getFont2()
        {
            return getStreamFromFile(ServerConfiguration.basePath + "assets\\fonts\\fontawesome-webfont.woff2", "application/font-woff2");
        }

        public Stream getFont3()
        {
            return getStreamFromFile(ServerConfiguration.basePath + "assets\\fonts\\fontawesome-webfont.ttf", "application/x-font-ttf");
        }

        public Stream getIndexPage()
        {
            if (!isAuthentificated())
                return getLoginPage();
            else
                return getStreamFromFile(ServerConfiguration.basePath + "index.html", GuiDLL.GuiConstants.HTML_TYPE);
        }

        public Stream getSubPage()
        {
            if (!isAuthentificated())
                return getLoginPage();
            else
                return getStreamFromFile(ServerConfiguration.basePath + "subpage.html", GuiDLL.GuiConstants.HTML_TYPE);
        }

        private static Stream getStreamFromFile(string filePath, string contentType)
        {
            var ctx = WebOperationContext.Current.IncomingRequest;
            
            Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] " + ctx.UriTemplateMatch.RequestUri.ToString());
            OutgoingWebResponseContext context =
                WebOperationContext.Current.OutgoingResponse;
            context.ContentType = contentType;
            try
            {
                byte[] file = File.ReadAllBytes(filePath);
                return new MemoryStream(file);
            }
            catch (FileNotFoundException fileNotFoundException)
            {
                return new MemoryStream(ASCIIEncoding.UTF8.GetBytes($"File not found. Description: {fileNotFoundException.Message}"));
            }
        }

        public IList<ModelDLL.WarehouseBook> getWarehouseSelect()
        {
            if (!isAuthentificated())
                return null;
            else
                return ModelOperations.Select<WarehouseBook>(WarehouseBook.SelectCommand, new DatabaseConnection());           
        }               
       
        public void postWarehouseInsert(System.IO.Stream s)
        {
            if (!isAuthentificated())
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return;
            }
            else
            {
                WarehouseBook warehouse = ModelOperations.GetFromJson<WarehouseBook>(s).FirstOrDefault();
                try
                {
                    ModelOperations.Insert(warehouse, new DatabaseConnection());
                }
                catch (SqlException ex)
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Conflict;
                    ctx.OutgoingResponse.StatusDescription = ex.Message;
                }
            }
        }

        public void getTableDelete(string tableName, string id)
        {
            if (!isAuthentificated())
            {
                return;
            }
            try
            {
                ModelOperations.ExecuteSqlCommand($"delete from {tableName} where ID='{id}'", new DatabaseConnection());

            }
            catch (SqlException ex)
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Conflict;
                ctx.OutgoingResponse.StatusDescription = ex.Message;
            }
        }

        public void postWarehouseUpdate(Stream s)
        {
            WarehouseBook warehouse = ModelOperations.GetFromJson<WarehouseBook>(s).FirstOrDefault();
            ModelOperations.Update(warehouse, new DatabaseConnection());
        }
    }
}
