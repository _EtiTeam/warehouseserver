﻿using NServiceBus;
using NServiceBus.Logging;
using NServiceBus.Persistence.Sql;
using ServiceBusDLL;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace WarehouseService.ServiceBus.Reservation
{
    class ReservationSaga :
        SqlSaga<ReservationSagaData>,
        IAmStartedByMessages<OrderCommitted>,
        IHandleMessages<OrderReservationRollback>,
        IHandleMessages<OrderFinished>
    {
        static ILog log = LogManager.GetLogger<ReservationSaga>();

        private const System.Data.IsolationLevel isolationLevel = System.Data.IsolationLevel.RepeatableRead;

        protected override string CorrelationPropertyName => nameof(Data.ReservationId);

        protected override void ConfigureMapping(IMessagePropertyMapper mapper)
        {
            // it configures all messages
            mapper.ConfigureMapping<MessageBase>(message => message.Id);
        }


        public Task Handle(OrderCommitted message, IMessageHandlerContext context)
        {
            log.Info($"Fetched CommitedOrder: { message.Id }");
            Data.ReservationId = message.Id; // it is set automatically 
            Data.OrderSagaId = message.OrderSagaId;
            Data.BookId = message.BookId;

            var connection = new DatabaseConnection().SqlConnection;
            SqlTransaction transaction = null;
            try
            {
                connection.Open();
                transaction = connection.BeginTransaction(isolationLevel);
                SqlCommand cmd = connection.CreateCommand();
                cmd.Transaction = transaction;

                var book = ModelDLL.ModelOperations.SelectUsingEstablishConnection<ModelDLL.WarehouseBook>(Data.BookId, cmd);
                if (book.Amount <= 0)
                {
                    log.Info($"NOT reserved. ReservationId={ Data.ReservationId }");
                    MarkAsComplete();
                    var replyMessage = new OrderItemNotReserved
                    {
                        Id = message.OrderSagaId,
                        ConfirmationId = Data.ReservationId
                    };
                    return context.Send(Data.Originator, replyMessage);
                }
                else
                {
                    --book.Amount;
                    ModelDLL.ModelOperations.UpdateUsingEstablishConnection(book, cmd);

                    log.Info($"RESERVED. ReservationId={ Data.ReservationId }");
                    var replyMessage = new OrderItemReserved
                    {
                        Id = message.OrderSagaId,
                        ReservationId = Data.ReservationId
                    };
                    return context.Send(Data.Originator, replyMessage);
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                transaction = null;
                throw ex;
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Commit();
                }
                connection.Close();
            }
        }

        public Task Handle(OrderReservationRollback message, IMessageHandlerContext context)
        {
            log.Info($"Fetched OrderRollback: { message.Id } ||| FROM OrderSaga: { Data.OrderSagaId }");

            var connection = new DatabaseConnection().SqlConnection;
            SqlTransaction transaction = null;
            try
            {
                connection.Open();
                transaction = connection.BeginTransaction(isolationLevel);
                SqlCommand cmd = connection.CreateCommand();
                cmd.Transaction = transaction;

                var book = ModelDLL.ModelOperations.SelectUsingEstablishConnection<ModelDLL.WarehouseBook>(Data.BookId, cmd);
                ++book.Amount;
                ModelDLL.ModelOperations.UpdateUsingEstablishConnection(book, cmd);

                MarkAsComplete();
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                transaction = null;
                throw ex;
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Commit();
                }
                connection.Close();
            }
        }

        public Task Handle(OrderFinished message, IMessageHandlerContext context)
        {
            log.Info($"Fetched OrderFinished: { message.Id } ||| FROM OrderSaga: { Data.OrderSagaId }");

            MarkAsComplete();
            return Task.CompletedTask;
        }
    }
}
