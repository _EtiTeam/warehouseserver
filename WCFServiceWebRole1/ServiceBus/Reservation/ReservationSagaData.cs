﻿using NServiceBus;
using System;

namespace WarehouseService.ServiceBus.Reservation
{
    class ReservationSagaData : ContainSagaData
    {
        override public Guid Id { get; set; }

        public Guid ReservationId { get; set; }

        public Guid OrderSagaId { get; set; }

        public int BookId { get; set; }
    }
}
