﻿using System;
using NServiceBus;
using System.ServiceModel;

namespace SpeditionServerStarter
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    class WarehouseServiceImpl : 
        WarehouseService.WarehouseService
    {
        public override IEndpointInstance NServiceBusEndpoint { get; set; }

        public WarehouseServiceImpl(IEndpointInstance endpoint)
        {
            //this.endpoint = endpoint;
        }
    }
}
